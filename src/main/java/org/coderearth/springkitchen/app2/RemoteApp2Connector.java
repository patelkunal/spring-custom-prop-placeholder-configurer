package org.coderearth.springkitchen.app2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by kunal_patel on 04/08/17.
 */
@Component
public class RemoteApp2Connector {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteApp2Connector.class);

    @Value("${remoteapp2.username}")
    private String username;

    @Value("${remoteapp2.password}")
    private String password;

    public void doSomething() {
        checkInit();
    }

    @PostConstruct
    public void postInit() {
        checkInit();
    }

    private void checkInit() {
        LOGGER.info("<username, password> ==> <{}, {}>", username, password);
    }

}
