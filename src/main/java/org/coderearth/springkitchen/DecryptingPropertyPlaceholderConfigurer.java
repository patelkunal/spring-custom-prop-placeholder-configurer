package org.coderearth.springkitchen;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.util.Base64Utils;
import org.springframework.util.ObjectUtils;

import java.util.Enumeration;
import java.util.Properties;

/**
 * Created by kunal_patel on 04/08/17.
 */
public class DecryptingPropertyPlaceholderConfigurer extends PropertyPlaceholderConfigurer {

    private static final Logger LOGGER = LoggerFactory.getLogger(DecryptingPropertyPlaceholderConfigurer.class);

    @Override
    protected void convertProperties(Properties props) {
        final Enumeration<?> enumeration = props.propertyNames();
        while (enumeration.hasMoreElements()) {
            String propertyName = (String) enumeration.nextElement();

            if (propertyName.equalsIgnoreCase("remoteapp1.password") || propertyName.equalsIgnoreCase("remoteapp2.password")) {
                LOGGER.trace("Decrypting property ({})", propertyName);
                String propertyValue = props.getProperty(propertyName);
                final String decodedValue = new String(Base64Utils.decode(propertyValue.getBytes())).trim();
                if (!ObjectUtils.nullSafeEquals(propertyValue, decodedValue)) {
                    props.setProperty(propertyName, decodedValue);
                }
            }
        }
    }
}
