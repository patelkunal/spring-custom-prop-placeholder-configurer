package org.coderearth.springkitchen.app1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kunal_patel on 04/08/17.
 */
public class RemoteApp1ConnectorPool {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteApp1ConnectorPool.class);

    private final String username;
    private final String password;

    private final Set<RemoteApp1Connector> pool = new HashSet<>();

    public RemoteApp1ConnectorPool(String username, String password, int poolSize) {
        this.username = username;
        this.password = password;
        for (int i = 0; i < poolSize; i++) {
            pool.add(new RemoteApp1Connector(username, password));
        }
    }

    @PostConstruct
    public void checkInit() {
        LOGGER.info("Initialized pool of size ({}) with config <username, password> = <{}, {}>", pool.size(), username, password);
    }

}
