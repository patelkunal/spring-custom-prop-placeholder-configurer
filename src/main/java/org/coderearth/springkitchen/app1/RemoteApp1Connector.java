package org.coderearth.springkitchen.app1;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by kunal_patel on 04/08/17.
 */
@Component
public class RemoteApp1Connector {

    private static final Logger LOGGER = LoggerFactory.getLogger(RemoteApp1Connector.class);

    private String username;
    private String password;

    public RemoteApp1Connector(@Value("${remoteapp1.username}") String username, @Value("${remoteapp1.password}") String password) {
        this.username = username;
        this.password = password;
    }

    public void doSomething() {
        checkInit();
    }

    @PostConstruct
    public void postInit() {
        checkInit();
    }

    private void checkInit() {
        LOGGER.info("<username, password> ==> <{}, {}>", username, password);
    }

}
